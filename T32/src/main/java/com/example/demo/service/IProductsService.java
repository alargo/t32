package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Products;

public interface IProductsService {
public List<Products> listarProducts(); //Listar All 
	
	public Products guardarProduct(Products product);	//Guarda un cliente CREATE
	
	public Products productXid(int id); //Leer datos de un cliente READ
	
	public Products actualizarProduct(Products product); //Actualiza datos del cliente UPDATE
	
	public void eliminarProduct(int id);// Elimina el cliente DELETE

}
